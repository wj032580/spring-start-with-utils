# 初始化springboot项目

#### 介绍
springboot项目,加入一些常用工具,方便以后直接使用

#### 软件架构
* 基础框架: springBoot
* 数据库: JPA  阿里巴巴druid
* 接口文档: swagger
* 日志等工具: lombok插件


#### 使用说明
```
本项目主要提供接口的通用性方法, 只需要在controller中继承BaseController即可自动实现如下几种方法: 
/XXX/saveOne  POST      : 保存单个数据 
/XXX/saveList POST      : 批量保存数据
/XXX/delete   delete    : 根据ID删除数据
/XXX/getList  GET       : 查询数据, 简单查询, 参数为名为字段名
/XXX/getList  GET       : 查询数据, 复杂查询, 参数为 参数名 + _ + 参数查询方式 如: age_greaterThen=10, 年龄大于10
        复杂查询参数名设置规则: 
            数字类, 时间类: 
                >   :  greaterThen 
                >=  :  greaterThenEqual
                <   :  lessThen
                <=  :  lessThenEqual
                =   :  equal
                日期格式    : yyyy-MM-dd HH:mm:ss
            字符类
                like        :  contains
                startWith   :  startWith
                endWith     :  endWith
                null判断    :  isNull notNull 参数值一律为true
                in          : in 多个值之间用,分隔

2.基础工具类  com.wj.init.utils
    DateUtils: 常用时间操作方法
    HttpUtils: 调用Http请求的方法
    JsonUtils: 实体类 JSON之间相互转换
    Utils:   : 常用方法, 目前有md5加密方法
```
            

 




