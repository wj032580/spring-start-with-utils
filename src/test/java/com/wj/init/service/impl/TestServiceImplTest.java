package com.wj.init.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wj.init.InitApplicationTests;
import com.wj.init.entity.TestEntity;
import com.wj.init.repository.TestRepository;
import com.wj.init.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

@Slf4j
public class TestServiceImplTest extends InitApplicationTests {

    @Resource
    private TestService service;

    @Resource
    private TestRepository repository;

    @Resource
    private ObjectMapper mapper;

    @Test
    public void test() {
        List<String> firstName = Arrays.asList("吴", "张", "王", "徐", "李");
        List<String> name = Arrays.asList("键", "伟", "量", "的", "怕");
        Random random = new Random();
        firstName.forEach(x -> name.forEach(y -> {
            TestEntity model = new TestEntity();
            model.setCreateDate(new Date());
            model.setName(x + y);
            model.setAge(random.nextInt(100));
            service.save(model);
        }));
    }

    @Test
    public void get() throws Exception {
        TestEntity model = new TestEntity();
        model.setName("伟");
        log.info(mapper.writeValueAsString(service.getList(PageRequest.of(0, 10), model)));
    }

    @Test
    public void delete() {

        service.deleteById("40280a816cf11c79016cf11c80430000");
    }


    @Test
    public void deleteAndSave() {
        TestEntity model = new TestEntity();
        model.setName("伟");
        List<TestEntity> list =  service.getList(PageRequest.of(0, 10), model);
        repository.deleteInBatch(list);
//        repository.save(model);

    }

}
