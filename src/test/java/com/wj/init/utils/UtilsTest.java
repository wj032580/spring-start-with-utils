package com.wj.init.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.wj.init.InitApplicationTests;
import com.wj.init.entity.TestEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.*;

@Slf4j
public class UtilsTest {

    @Test
    public void md5() {
        log.info(Utils.md5("123"));
    }

    @Test
    public void a() throws Exception{
        List<TestEntity> a = new ArrayList<>();
        TestEntity b = new TestEntity();
        b.setId(UUID.randomUUID().toString());
        a.add(b);
        TestEntity c = new TestEntity();
        c.setId(UUID.randomUUID().toString());
        a.add(c);
        log.info(JsonUtil.json(a));

    }

    @Test
    public void b() throws Exception {
        String j = "[{\"id\":\"7e1a7f92-12f3-4607-835f-7ff61d3e604e\",\"createDate\":null,\"updateDate\":null,\"name\":null,\"age\":null,\"account\":null},{\"id\":\"9816a3fb-cf8e-4e44-bff7-fb1f647ab474\",\"createDate\":null,\"updateDate\":null,\"name\":null,\"age\":null,\"account\":null}]";
        List<TestEntity> list = JsonUtil.jsonToList(j, TestEntity.class);
//        List<TestEntity> list = Utils.MAPPER.readValue(j, new TypeReference<List<TestEntity>>(){});
        list.forEach(x ->log.info(x.getId()));
    }

    @Test
    public void c() throws Exception {
        String s = "{\"id\":\"e6b829af-708a-47fb-8c10-14ae13d03ffb\",\"createDate\":null,\"updateDate\":null,\"name\":null,\"age\":null,\"account\":null}";
        TestEntity model = JsonUtil.jsonToEntity(s, TestEntity.class);
        log.info(model.getId());
    }

    @Test
    public void d() throws Exception {
        String s = "{\"id\":\"e6b829af-708a-47fb-8c10-14ae13d03ffb\",\"createDate\":null,\"updateDate\":null,\"name\":null,\"age\":null,\"account\":null}";
        Map model = JsonUtil.jsonToMap(s);
        log.info(model.get("id").toString());
    }


    public static void main(String[] args) {
        System.out.print(Result.SUCCESS);
    }

}
