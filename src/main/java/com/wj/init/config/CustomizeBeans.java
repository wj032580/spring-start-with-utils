package com.wj.init.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 吴健
 * @date 2019/9/2 11:18
 * @desc 自定义bean
 */
@Configuration
public class CustomizeBeans {

    /**
     * json操作类
     * @return
     */
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
