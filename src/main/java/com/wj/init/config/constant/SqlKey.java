package com.wj.init.config.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 吴健
 * @date 2020/3/5 15:05
 * @desc 查询关键字
 */
@Getter
@AllArgsConstructor
public enum  SqlKey {
    // >
    GREATER_THEN("greaterThen"),
    // >=
    GREATER_THEN_EQUAL("greaterThenEqual"),
    // <
    LESS_THEN("lessThen"),
    //<=
    LESS_THEN_EQUAL("lessThenEqual"),
    // =
    EQUAL("equal"),
    // like
    CONTAINS("contains"),
    // like
    START_WITH("startWith"),
    //like
    END_WITH("endWith"),
    // is null
    IS_NULL("isNull"),
    // is not null
    NOT_NULL("notNull"),
    //in
    IN("in");

    private final String value;
}
