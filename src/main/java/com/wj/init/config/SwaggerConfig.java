package com.wj.init.config;

import com.wj.init.InitApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author 吴健
 * @date 2019/9/7 16:21
 * @desc swagger配置
 */
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(InitApplication.class.getPackage().getName() + ".controller"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * swagger 基础描述信息
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("swagger api 接口测试")
                .description("springBoot项目基础工具功能搭建")
                .termsOfServiceUrl("http://192.168.31.208:9003/excel/main/index")
                .contact(new Contact("wu-jian", "http://192.168.31.208:9003/excel/main/index", "1181908851@qq.com"))
                .version("1.0")
                .build();
    }
}
