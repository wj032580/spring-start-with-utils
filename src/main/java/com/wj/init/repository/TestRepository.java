package com.wj.init.repository;

import com.wj.init.entity.TestEntity;

/**
 * @author 吴健
 * @date 2019/9/2 16:17
 * @desc
 */
public interface TestRepository extends BaseRepository<TestEntity> {
}
