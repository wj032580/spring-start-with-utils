package com.wj.init.controller;

import com.wj.init.entity.TestEntity;
import com.wj.init.service.BaseService;
import com.wj.init.service.TestService;
import com.wj.init.utils.Result;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ：吴健
 * @date ：2019-09-04 21:14
 * @desc ：
 */
@RestController
@Api(tags = "测试接口")
@RequestMapping(value = "test", produces = "application/json")
public class TestController extends BaseController<TestEntity> {

    @Resource
    private TestService service;

    @Override
    BaseService<TestEntity> getService() {
        return service;
    }

    @RequestMapping(value = "error", method = RequestMethod.GET)
    public String error() {
        Integer.parseInt("asd");
        return Result.SUCCESS;
    }

}
