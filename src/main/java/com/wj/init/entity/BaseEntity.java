package com.wj.init.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author 吴健
 * @date 2019/9/2 17:08
 * @desc 基础实体类
 */
@Data
@MappedSuperclass
@ApiModel
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity {

    /**
     * 主键,自动生成32为UUID
     */
    private String id;
    /**
     * 数据创建时间, jpa自动生成
     */
    private Date createDate;
    /**
     * 更新时间,JPA注解自动生成
     */
    private Date updateDate;

    @Id
    @Column(name = "id", length = 32)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    @ApiModelProperty(value = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @CreatedDate
    @Column(name = "create_date", updatable = false)
    @ApiModelProperty(value = "创建时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Basic
    @LastModifiedDate
    @Column(name = "update_date")
    @ApiModelProperty(value = "更新时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
