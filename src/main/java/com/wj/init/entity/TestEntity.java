package com.wj.init.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * @author 吴健
 * @date 2019/9/2 16:15
 * @desc
 */
@Entity
@Table(name = "test")
@ApiModel
public class TestEntity extends BaseEntity {

    private String name;
    private Integer age;
    private Double account;

    @Basic
    @Column(name = "name", length = 50)
    @ApiModelProperty(value = "名称")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "age", columnDefinition = "decimal(3)")
    @ApiModelProperty(value = "年龄", example="0", dataType = "int")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Basic
    @Column(name = "account", columnDefinition = "decimal(5, 2)")
    @ApiModelProperty(value = "存款", example = "0", dataType = "double")
    public Double getAccount() {
        return account;
    }

    public void setAccount(Double account) {
        this.account = account;
    }
}
