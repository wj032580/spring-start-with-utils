package com.wj.init.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author 吴健
 * @date 2019/11/9 11:48
 * @desc json转换工具
 */
@Slf4j
public class JsonUtil {

    /**
     * 数据JSON化
     * @param o 需要转换的数据
     * @return
     * @throws Exception
     */
    public static String json(Object o) throws Exception {
        return Utils.MAPPER.writeValueAsString(o);
    }

    /**
     * json转集合
     * @param json json
     * @param <T> 需要转换的类型
     * @return
     */
    public static <T> List<T> jsonToList(String json, Class<T> tClass) throws Exception {
        JsonNode nodes = Utils.MAPPER.readTree(json);
        List<T> list = new ArrayList<>();
        AtomicBoolean error = new AtomicBoolean(false);
        nodes.elements().forEachRemaining(x -> {
            try {
                list.add(Utils.MAPPER.readValue(x.toString(), tClass));
            } catch (Exception  e) {
                log.error("json序列化失败");
                error.set(true);
            }
        });
        return error.get()? null : list;
    }

    /**
     * json转换实体
     * @param json json
     * @param tClass 需要转换的类型
     * @param <T> T
     * @return
     * @throws Exception
     */
    public static <T> T jsonToEntity(String json, Class<T> tClass) throws Exception {
        return Utils.MAPPER.readValue(json, tClass);
    }

    /**
     * JSON转MAP
     * @param json json
     * @return
     * @throws Exception
     */
    public static Map jsonToMap(String json) throws Exception {
        return Utils.MAPPER.readValue(json, new TypeReference<Map>(){});
    }
}
