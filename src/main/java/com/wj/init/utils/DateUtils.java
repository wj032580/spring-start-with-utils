package com.wj.init.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author ：吴健
 * @date ：10:51 2018/11/26
 * @desc ：获取时间工具类
 */
public class DateUtils {

    public static final String PATTERN_1 = "yyyyMMddHHmmss";
    public static final String PATTERN_2 = "yyyy/MM/dd HH:mm:ss";
    public static final String PATTERN_4 = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_3 = "yyyy-MM-dd";
    public static final String PATTERN_7 = "yyyy.MM.dd";
    public static final String PATTERN_5 = "yyyyMMdd";
    public static final String PATTERN_6 = "yyyyMM";

    /**
     * 将时间Date转换为String
     * @param date      时间date
     * @param pattern   String式样
     * @return
     */
    public static String getStringDate(Date date, String pattern) {
        return new SimpleDateFormat(pattern).format(date);
    }

    /**
     * 根据String格式获取Date格式时间数据
     * @param date
     * @param pattern
     * @return
     * @throws Exception
     */
    public static Date getDateFromString (String date, String pattern) throws Exception {
        return new SimpleDateFormat(pattern).parse(date);
    }

    /**
     * 获取当前时间String
     * @param pattern String式样
     * @return
     */
    public static String getNowDateString(String pattern) {
        return getStringDate(new Date(), pattern);
    }

    /**
     * 对月数加i
     * @param date 原始时间
     * @param i     减动数
     * @return
     */
    public static Date addMonths(Date date, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, i);
        return calendar.getTime();
    }

    /**
     * 对年份加i
     * @param date 原始时间
     * @param i     减动数
     * @return
     */
    public static Date addYears(Date date, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, i);
        return calendar.getTime();
    }

    /**
     * 设置时间月份
     * @param date 时间
     * @param field 元素
     * @param i 增加数
     * @return
     */
    public static Date setField(Date date, int field, int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(field, i);
        return calendar.getTime();
    }

    /**
     * 获取时间元素
     * @param date 时间
     * @param field 元素,Calender获取
     * @return
     */
    public static int getField(Date date, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(field);
    }

    /**
     * 比较时间大小, 1 > 2 ,true
     * @param date1 时间
     * @param date2 时间
     * @return
     */
    public static boolean compare(Date date1, Date date2){
         return date1.compareTo(date2) > 0;
    }
}
