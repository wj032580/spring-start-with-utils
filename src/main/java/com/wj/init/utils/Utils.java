package com.wj.init.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * @author ：吴健
 * @date ：21:16 2019-09-03
 * @desc ：公用方法
 */
public class Utils {

    public static final ObjectMapper MAPPER = new ObjectMapper();
    static {
        MAPPER.setDateFormat(new SimpleDateFormat(DateUtils.PATTERN_4));
        MAPPER.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    }

    /**
     * md5加密
     * @param s 要加密的字符串
     * @return sign
     */
    public static String md5 (String s) {
        return DigestUtils.md5DigestAsHex(s.getBytes(StandardCharsets.UTF_8));
    }
}
