package com.wj.init.service;

import com.wj.init.entity.TestEntity;

/**
 * @author 吴健
 * @date 2019/9/2 16:17
 * @desc test测试
 */
public interface TestService extends BaseService<TestEntity> {
}
