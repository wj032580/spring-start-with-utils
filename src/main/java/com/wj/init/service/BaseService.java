package com.wj.init.service;

import com.wj.init.entity.BaseEntity;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author 吴健
 * @date 2019/9/2 15:18
 * @desc 基础接口, 分装通用方法
 */
public interface BaseService<T extends BaseEntity> {

    /**
     * 保存单个数据
     *
     * @param model data
     * @return
     */
    boolean save(T model);

    /**
     * 保存集合
     *
     * @param list data
     * @return
     */
    boolean save(List<T> list);

    /**
     * 根据主键删除ID
     *
     * @param id id
     * @return
     */
    void deleteById(String id);

    /**
     * 获取数据, 简单查询
     *
     * @param page  页码信息
     * @param model 查询条件
     * @return
     */
    List<T> getList(Pageable page, T model);

    /**
     * 获取数据, 复杂查询
     *
     * @param pageable page
     * @param params   查询条件
     * @return
     * @throws
     */
    List<T> getList(Pageable pageable, Map<String, String[]> params) throws Exception;

    /**
     * 获取数据,
     * 复杂查询
     *
     * @param pageable 分页信息
     * @param model 查询条件
     * @return
     * @throws Exception 异常
     */
    List<T> getList(Pageable pageable, Object model) throws Exception;
}
