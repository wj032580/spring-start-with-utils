package com.wj.init.service.impl;

import com.wj.init.entity.TestEntity;
import com.wj.init.repository.BaseRepository;
import com.wj.init.repository.TestRepository;
import com.wj.init.service.TestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 吴健
 * @date 2019/9/2 16:18
 * @desc
 */
@Service
public class TestServiceImpl extends BaseServiceImpl<TestEntity> implements TestService {

    @Resource
    private TestRepository repository;

    @Override
    BaseRepository<TestEntity> getRepo() {
        return repository;
    }
}
